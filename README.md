# Spring Data Cassandra REST API
## Containerized with Docker and Deployed to Google Kubernetes Engine.
### Clone of Spring Data Cassandra REST API (https://github.com/DataStax-Examples/spring-data-starter)

## Introduction:
This demo is a clone of Spring Data Cassandra REST API example on DataStax.
I will create a multi-stage Docker image for the application and deploy it to Kubernetes Cluster (Google Kubernetes Engine).
I have chosen to Docker-ize the application to practice deployment of micro-services in a Kubernetes cluster.

This application depends on a Cassandra cluster--I'm using Astra. You will need the login information and the secure connection bundle.
You will need a Docker Hub account in order to push the container image there.


## The steps are as follows:

### 1. Assume you have setup an account on Google Cloud Platform, login to Google Cloud Platform.
### 2. Start Kubernetes Cluster (GKE)
### 3. Connect to GPP Cluster
### 4. Clone the repo: git clone git@gitlab.com:pcng1995/spring-data-starter.git to the ./spring-data directory.
### 5. Once the repository is cloned, you will see the following files sub-folders:

    deploy/  Dockerfile   mvnw  mvnw.cmd  network/  pom.xml  README.md  rest  secrets/  src/

### 6. We will create a Kubernetes Secret to Cassandra credential information as below.
   Update the environment file secrets/astra-env.conf.

      ASTRA_DB_BUNDLE=<path to secure bundle>
      ASTRA_DB_USERNAME=<username>
      ASTRA_DB_REGION=<region>
      ASTRA_DB_PASSWORD=<password>
      ASTRA_DB_KEYSPACE=<keyspace>

### 7. Download the Cassandra secure connection bundle to ./secrets/astra-creds.zip

### 8. Dockerfile: Multi-stage build
    ---
    FROM adoptopenjdk/openjdk11 as build
    WORKDIR /workspace/app

    COPY mvnw .
    COPY .mvn .mvn
    COPY mvnw.cmd mvnw.cmd
    COPY pom.xml .
    COPY src src

    RUN ./mvnw install -DskipTests  
    RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

    FROM adoptopenjdk/openjdk11:jdk-11.0.9_11-alpine
    VOLUME /tmp
    ARG DEPENDENCY=/workspace/app/target/dependency
    COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
    COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
    COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
    ENTRYPOINT ["java","-cp","app:app/lib/*","com.datastax.examples.SpringDataCassandraApplication"]
    EXPOSE 8081/TCP
    EXPOSE 8082/TCP
    ...

### 9. Build the Docker container and push the image to hub.docker.com
  You will need to substitute your Docker Hub account ID.

     cd ./spring-data-starter
     # Build Docker image.
     docker build -t <docker-hub-account>/spring-data-starter .

     # Login to Docker Hub and push image.
     docker push <docker-hub-account>/spring-data-starter:latest


### 10. Assuming your cluster image is available: Optionally, create a    Kubernetes namespace: dev, and switch to that namespace.
    kubectl create namespace dev
    kubectl config set-context --current --namespace=dev

### 11. Assuming you cluster is available, deploy the following Kubernetes Secrets.
  These secrets are not stored securely, but it's suitable for demo purposes.

    cd ./secrets
    # Secret to hold the Astra secure connection bundle.
    kubectl create secret generic astra-creds --from-file=./astra-creds.zip

    # Secret to hold credentials.
    kubectl create secret generic astra-env --from-env-file=./astra-env.conf

    # Check that the secrets have been created:
    kubectl get secrets
    ---
    NAME                  TYPE                                  DATA   AGE
    astra-creds           Opaque                                1      93m
    astra-env             Opaque                                5      91m


### 12. Deployment YAML file deploy/deploy.yaml


    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: spring-data  
      labels:
        app: spring-data
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: spring-data
          tier: backend
      strategy:
        type: Recreate
      template:
        metadata:
          labels:
            app: spring-data
            tier: backend
        spec:
          containers:
          - name: backend
            ''' SET THE <docker-hub-id>
            image: <docker-hub-id>/spring-data-starter:latest
            ''' ADD THE CREDENTIALS TO THE ENVIRONMENT OF THE CONTAINER
            envFrom:
            - secretRef:
                name: astra-env
            # EXPOSE THE CONTAINER PORTS
            ports:
            - containerPort: 8081
              name: server
            - containerPort: 8082
              name: mgmt
            # MOUNT THE VOLUME TO THE CONTAINER
            volumeMounts:
            - name: astra-creds
              mountPath: /etc/conf
          # MOUNT SECURE BUNDLE TO THE VOLUME
          volumes:
          - name: astra-creds
            secret:
              secretName: astra-creds
      ---             
      apiVersion: v1
      # CREATE A SERVERICE TO EXPOSE TO CONTAINER PORTS EXTERNALLY
      kind: Service
      metadata:
        name: spring-data    ## Service name
      spec:
        type: LoadBalancer
        selector:
          app: spring-data
          tier: backend
        ports:
          - protocol: TCP
            port: 8081
            name: server
          - protocol: TCP
            port: 8082
            name: mgmt
      ...



### 13. Deploy the application to the GKE cluster.


    cd ./deploy
    kubectl create -f ./deploy.yaml

    It should take a couple minutes for the deployment to be completed.

    kubectl get deploy,svc,po

    NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/spring-data   1/1     1            1           97m

    NAME                  TYPE           CLUSTER-IP   EXTERNAL-IP     PORT(S)                         AGE
    service/spring-data   LoadBalancer   10.8.0.209   34.70.212.213   8081:30982/TCP,8082:30020/TCP   97m

    NAME                               READY   STATUS    RESTARTS   AGE
    pod/spring-data-67c5fbd598-kq9kv   1/1     Running   0          97m


  The EXTERNAL-IP should be populated (34.70.212.213, yours will differ), and not PENDING

### 14. At this point we are ready to exercise the application.
    Examine the Swagger interface: Paste the following into your browser: http://<EXTERNAL_IP>:8081/swagger-ui/

### 15. We can also access the service from inside the cluster.



    kubectl run client1 --rm -it --image=radial/busyboxplus -- /bin/sh


    curl -X POST "http://spring-data:8081/orders" -H "accept: */*" -H "Content-Type: application/json" \
    -d '{"addedToOrderTimestamp": "2020-10-27T02:07:11.497Z","key":{"orderId":"9880ff18-18a1-11eb-adc1-0242ac120002",
    "productId":"9880ff18-18a1-11eb-adc1-0242ac120002"},"productName":"Camera","productPrice":3500.84,"productQuantity":2}'

    / #
    {"key":{"orderId":"9880ff18-18a1-11eb-adc1-0242ac120002","productId":"9880ff18-18a1-11eb-adc1-0242ac120002"},"productQuantity":2,"productName":"Camera","productPrice":3500.84,"addedToOrderTimestamp":"2020-10-27T02:07:11.497Z"}  

    -------

    / # curl -X GET "http://spring-data:8081/orders/9880ff18-18a1-11eb-adc1-0242ac120002" -H "accept: */*"
        [{"productName":"Camera","productPrice":3500.84}]

    / # curl -X GET "http://spring-data:8081/orders/76272f10-1896-11eb-adc1-0242ac120002" -H "accept: */*"

    ## Exit client1 pod:
    / # Ctrl-D
    -------
### 16. Clean-up
Change namespace

    kubectl config set-context --current --namespace=default
    kubectl delete namespace dev

Otherwise, manually delete components:

    kubectl delete -f deploy.yaml

    kubectl delete secret astra-creds, astra-env


